# jskm 0.2.1

* Can run when reactive data

# jskm 0.2.0

## New function

* `svyjskm` : Weighted Kaplan-Meier plot - `svykm.object` in **survey** package

# jskm 0.1.0

* `frailty`, `cluster` options and their **p value**